jQuery.noConflict();
if(typeof WP_DATA == "undefined" || typeof WP_DATA.ajax_url == "undefined") {
    window.ajax_url = '/wp/wp-admin/admin-ajax.php';
} else {
    window.ajax_url = WP_DATA.ajax_url;
}
jQuery( document ).ready( function ( $ ) {


    $("#signup-form").validate({
        submitHandler: function(form) {
            $("#mailchimp_response").html("");
            $("#signup-form").find(".loader").show();
            $.post(window.ajax_url, $("#signup-form").serialize(), function(data){
                $("#signup-form").find("input[type=submit]").attr('disabled', true);
                var ret = $.parseJSON(data);
                $("#signup-form").find(".loader").hide();
                if (typeof ret.error !== 'undefined') {
                    $("#mailchimp_response").html("<h2>" + ret.error + "</h2>").css('color', '#0099a8');

                } else {
                    $( "#signup-form fieldset" ).animate({
                        opacity: 0
                    }, 500, function() {
                        $("#mailchimp_response").html("<h2>You have successfully been subscribed!");
                    });

                    $("#signuptext").val('');
                }
            });
            $("#signup-form").find("input[type=submit]").attr('disabled', false);
            return false;
        }
    });
});