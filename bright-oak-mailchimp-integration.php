<?php
/*
 * Plugin Name: Bright Oak Mailchimp Integration
 * Version: 1.0.1
 * Description: Allows signup to a specified list.
 * Author: Rory McDaniel
 * Requires at least: 4.0
 * Tested up to: 4.4.2
 *
 * Text Domain: bright-oak-mailchimp-integration
 *
 * @author Rory McDaniel
 * @since 1.0.1
 */

if ( ! defined( 'ABSPATH' ) ) exit;


require_once( 'includes/class-bright-oak-mailchimp-integration.php' );
// Load plugin class files

require_once( 'includes/class-bright-oak-mailchimp-integration-settings.php' );


// Load plugin libraries
require_once( 'includes/lib/class-bright-oak-mailchimp-integration-api.php' );


/**
 * Returns the main instance of Bright_Oak_Mailchimp_Integration to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object Bright_Oak_Mailchimp_Integration
 */
function Bright_Oak_Mailchimp_Integration () {
	$instance = Bright_Oak_Mailchimp_Integration::instance( __FILE__, '1.0.1' );

	if ( is_null( $instance->settings ) ) {
		$instance->settings = Bright_Oak_Mailchimp_Integration_Settings::instance( $instance );
	}

	return $instance;
}

Bright_Oak_Mailchimp_Integration();
